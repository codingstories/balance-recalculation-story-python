class MultiTariffServiceStub:
    def __init__(self, tariffs):
        self.tariffs = tariffs

    def get_tariffs(self):
        return self.tariffs
